# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class TableContent(models.Model):
    class Meta:
        verbose_name = 'Запись'
        verbose_name_plural = 'Записи'

    title = models.CharField(u'Название записи', max_length=100)
    fieldA = models.CharField(blank=True, max_length=100)
    fieldB = models.CharField(blank=True, max_length=100)
    fieldC = models.CharField(blank=True, max_length=100)
    fieldD = models.IntegerField(blank=True, null=True)
    fieldE = models.BooleanField(blank=True)

    def __unicode__(self):
        return self.title
