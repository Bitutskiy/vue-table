from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='table_vue'),
    url(r'^full_table$', views.return_table),
    url(r'^save_row$', views.save_row),
]
