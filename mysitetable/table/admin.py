# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.

from .models import TableContent


class TableAdminForm(admin.ModelAdmin):
    fields = [
        'title',
        'fieldA',
        'fieldB',
        'fieldC',
        'fieldD',
        'fieldE',
    ]
    list_display = ('title',
                    'fieldA',
                    'fieldB',
                    'fieldC',
                    'fieldD',
                    'fieldE',
                    )
admin.site.register(TableContent, TableAdminForm)