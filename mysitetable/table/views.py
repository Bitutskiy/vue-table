# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

# Create your views here.
from django.template.response import TemplateResponse
from django.http import HttpResponse
from django.core import serializers
from json import loads
from django.template import loader
from django.contrib.auth.forms import UserCreationForm
from django.views.generic.edit import CreateView
from django.http import JsonResponse

from .models import TableContent


def index(request):
    # template = loader.get_template('table/index.html')
    return TemplateResponse(request, 'table/index.html', {})


def return_table(request):
    data = serializers.serialize("json", TableContent.objects.all())
    return HttpResponse(data)


def save_row(request):
    data = loads(request.body)['body']
    obj = TableContent.objects.get(pk=data['pk'])
    data = data['fields']
    obj.fieldA = data['fieldA']
    obj.fieldB = data['fieldB']
    obj.fieldC = data['fieldC']
    obj.title = data['title']
    try:
        obj.fieldD = int(data['fieldD'])
        obj.fieldE = bool(data['fieldE'])
    except ValueError:
        return HttpResponse()  # response with error
    obj.save()
    return HttpResponse()  # response correct


# class SignUpView(CreateView):
#     template_name = 'core/signup.html'
#     form_class = UserCreationForm