
Vue.component('my-table', {
    template: `<div class="table">
                    <table id="mytable">

                        <div id="modal">
                            <modal v-if="showModal" @close="showModal = false">
                                <h3 slot="header">#{{ title }}</h3>
                                <div v-for="item in items" slot="body">
                                    <div v-if="item.pk==title">
                                        <table id="modal_form_table" v-model="senderData=item">
                                            <th class="header">Key</th>
                                            <th class="right-header">Value</th>
                                            <tr v-for="key in columns" :class="{ tablerow : false }">
                                                <td>{{ key }}</td> <td> <input v-model="item.fields[key]"></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div slot="footer">
                                    Click save for send changes on server
                                    <button class="modal-default-button" @click="saveData">
                                        Save
                                    </button>
                                </div>
                            </modal>
                        </div>

                    <caption>{{ table_name }}</caption>
                        <tr>
                            <th v-for="key in columns">
                                {{ key }}
                            </th>
                        </tr>
                    <tbody>
                        <tr v-for="entry in items">
                            <td v-for="key in columns" class='tablerow' @click="modal(entry.pk)">
                                <div>
                                    {{entry.fields[key]}}
                                </div>
                            </td>
                        </tr>
                    </tbody>
                     </table>
                </div>`,
    data() {
        return {
            table_name: 'Hello! I am interactive table',
            columns: ['title', 'fieldA', 'fieldB', 'fieldC', 'fieldD', 'fieldE'],
            showModal: false,
            senderData: {},
            title: '',
            items: {},
            errors: [],
        }
    },

    created: function() {
        axios.get('/table/full_table'
            ).then(response => this.items = response.data)
             .catch( e => {
             this.errors.push(e)
             })
    },
    methods: {
        modal(id) {
            this.showModal = true;
            this.title = id;
        },
        saveData() {
            this.showModal = false;
            axios.post('/table/save_row', {
                body: this.senderData

            }).then(response => {})
              .catch(e => {
                this.errors.push(e)
              })
            this.senderData = {}
        }
    }
})

Vue.component('modal', {
  template: `<transition name="modal">
    <div class="modal-mask">
      <div class="modal-wrapper">
        <div class="modal-container">

          <div class="modal-header">
            <slot name="header">
              default header
            </slot>
          </div>

          <div class="modal-body">
            <slot name="body">
              default body
            </slot>
          </div>

          <div class="modal-footer">
            <slot name="footer">

              <button class="modal-default-button" @click="$emit('close')">
                Close
              </button>
            </slot>
          </div>
        </div>
      </div>
    </div>
  </transition>`
})